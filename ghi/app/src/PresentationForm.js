import React, {useEffect, useState} from 'react';



    function PresentationForm() {
        const [presenterName, setPresenterName] = useState('');
        const [presenterEmail, setPresenterEmail] = useState('');
        const [companyName, setCompanyName] = useState('');
        const [title, setTitle] = useState('');
        const [synopsis, setSynopsis] = useState('');
        const [conference, setConference] = useState('');
        // const [formData, setFormData] = useState('')
        const [conferences, setConferences] = useState([]);

        const handlePresenterNameChange = (event) => {
            const value = event.target.value;
            setPresenterName(value);
          }
        const handlePresenterEmailChange = (event) => {
            const value = event.target.value;
            setPresenterEmail(value);
        }
        const handleCompanyNameChange = (event) => {
            const value = event.target.value;
            setCompanyName(value);
        }
        const handleTitleChange = (event) => {
            const value = event.target.value;
            setTitle(value);
        }
        const handleSynopsisChange = (event) => {
            const value = event.target.value;
            setSynopsis(value);
        }
        const handleConferenceChange = (event) => {
            const value = event.target.value;
            setConference(value);
        }
        // const handleFormDataChange = (event) => {
        //     const value = (event.target.value)
        //     setFormData(value)
        // }


        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};
            data.presenter_name = presenterName;
            data.presenter_email = presenterEmail;
            data.company_name = companyName;
            data.title = title;
            data.synopsis = synopsis;
            data.conference = conference;
            console.log(data);

            // const formData = new FormData();
            // const conferenceId = formData.conference;
            const selectTag = document.getElementById('conference');
            const conferenceId = selectTag.options[selectTag.selectedIndex].value;
            const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
            console.log(conferenceId);
            const fetchConfig = {
              method: "post",
              body: JSON.stringify(data),
              headers: {
                'Content-Type': 'application/json',
              },
            };

            const res = await fetch(presentationUrl, fetchConfig);
            if (res.ok) {
              const newPresentation = await res.json();
              console.log(newPresentation);

              setPresenterName('');
              setPresenterEmail('');
              setCompanyName('');
              setTitle('');
              setSynopsis('');
              setConference('');
            }
       }

          const fetchData = async () => {
          const url = 'http://localhost:8000/api/conferences/';

          const res = await fetch(url);

          if (res.ok) {
            const data = await res.json();
            setConferences(data.conferences);

          }
        }

        useEffect(() => {
          fetchData();
        }, []);

    return (
    <>
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit}
            id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange}
                value={presenterName}
                placeholder="Presenter name"
                required type="text"
                id="presenter_name"
                name="presenter_name"
                className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange}
                value={presenterEmail}
                placeholder="Presenter email"
                required type="email"
                id="presenter_email"
                name="presenter_email"
                className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange}
                value={companyName}
                placeholder="Company name"
                required type="text"
                id="company_name"
                name="company_name"
                className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handleTitleChange}
                  value={title}
                  placeholder="Title"
                  required type="text"
                  id="title"
                  name="title"
                  className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                  <textarea onChange={handleSynopsisChange}
                  value={synopsis}
                  className="form-control"
                  id="synopsis"
                  name="synopsis"
                  rows="4" ></textarea>
                </div>
              <div className="mb-3">
                <label htmlFor="conference" className="form-label">Conference</label>
                <select
                // onChange={handleFormDataChange}
                // value={formData}
                onChange={handleConferenceChange}
                value={conference}
                required id="conference"
                name="conference"
                className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                        return (
                        <option key={conference.name} value={conference.id} >
                            {conference.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>

    );
}

export default PresentationForm;